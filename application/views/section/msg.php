<?php
	$errormessage = $this->session->userdata('error_message');
	$successmessage = $this->session->userdata('success_message');
	$error = $this->session->userdata('error');

	if ($errormessage) {
		echo '
			<div class="box-content">
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button><strong>'.$errormessage.'</strong>
				</div>
			</div>';
		$this->session->unset_userdata('error_message');
	}

	if ($error) {
		echo '
			<div class="box-content">
				<div class="alert alert-error">
					<button type="button" class="close" data-dismiss="alert">×</button><strong>'.$error.'</strong>
				</div>
			</div>';
		$this->session->unset_userdata('error');
	}

	if ($successmessage) {
		echo '
			<div class="box-content">
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button><strong>'.$successmessage.'</strong>
				</div>
			</div>';
		$this->session->unset_userdata('success_message');
	}
?>				