			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url() ?>dashboard">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="<?php echo $student_info->id ?>">View Student</a>
					<i class="icon-angle-right"></i><?php echo $student_info->name ?>
				</li>
			</ul>

				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="" method="post">
						  <fieldset>
							<div class="control-group">
							  <label class="control-label" for="date01">Name</label>
							  <div class="controls">
								<input type="text" class="input-xlarge"  value="<?php echo $student_info->name ?>" disabled>
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="date01">Phone</label>
							  <div class="controls">
								<input type="text" class="input-xlarge"  value="<?php echo $student_info->phone ?>" disabled>
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="fileInput">Roll</label>
							  <div class="controls">
								<input type="text" class="input-xlarge"  value="<?php echo $student_info->roll ?>" disabled>
							  </div>
							</div>          

							<div class="form-actions">
							  <a href="<?php echo base_url() ?>manage-student" class="btn btn-primary">Back</a>
							  <button type="reset" class="btn">Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->