			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url() ?>dashboard">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="<?php echo base_url() ?>admin-add">Add Admin</a></li>
			</ul>

				<div class="box span11">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">

						<?php $this->load->view('section/msg') ?>

						<form class="form-horizontal" action="<?php echo base_url() ?>save-admin" method="post" enctype="multipart/form-data">
						  <fieldset>
							<div class="control-group">
							  <label class="control-label" for="date01">Admin Name</label>
							  <div class="controls">
								<input type="text" class="input-xlarge" name="name" id="date01">
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="date01">Admin Email</label>
							  <div class="controls">
								<input type="text" class="input-xlarge" name="email" id="date01">
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="date01">Password</label>
							  <div class="controls">
								<input type="password" class="input-xlarge" name="password" id="date01">
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="fileInput">Image</label>
							  <div class="controls">
								<input class="input-file uniform_on" id="fileInput" name="image" type="file">
							  </div>
							</div>          

							<div class="form-actions">
							  <button type="submit" class="btn btn-primary">Upload Image</button>
							  <button type="reset" class="btn">Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->