			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url() ?>dashboard">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="<?php echo base_url() ?>manage-student">Manage Student</a></li>
			</ul>

					<div class="box-content">

						<?php $this->load->view('section/msg') ?>

						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  		<th>SL No</th>
								  	<th>Student Name</th>
								  	<th>Student Phone No.</th>
								  	<th>Roll</th>
								  	<th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
						  	<?php 
						  		foreach ($all_student_info as $key => $student_info) {
						  	 ?>
							<tr>
								<td class="center"><?php echo $key+1 ?></td>
								<td class="center"><?php echo $student_info->name ?></td>
								<td class="center"><?php echo $student_info->phone ?></td>
								<td class="center"><?php echo $student_info->roll ?></td>
								
								<td class="center">
									<a class="btn btn-success" href="<?php base_url() ?>view-student/<?php echo $student_info->id ?>">
										<i class="halflings-icon white zoom-in"></i>  
									</a>
									<a class="btn btn-info" href="<?php base_url() ?>edit-student/<?php echo $student_info->id ?>">
										<i class="halflings-icon white edit"></i>  
									</a>
									<a class="btn btn-danger" href="<?php base_url() ?>delete-student/<?php echo $student_info->id ?>" onclick="confirm('Are you want to delete this?')">
										<i class="halflings-icon white trash"></i> 
									</a>
								</td>
							</tr>
							<?php } ?>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->