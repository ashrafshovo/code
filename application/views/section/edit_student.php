			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url() ?>dashboard">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="<?php base_url() ?>edit-student/<?php echo $student_info->id ?>">Edit Student</a>
					<i class="icon-angle-right"></i>
					<?php echo $student_info->id ?>
				</li>
			</ul>

			<?php $this->load->view('section/msg'); ?>

				<div class="box span11">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>

					<div class="box-content">
						<form class="form-horizontal" action="<?php echo base_url() ?>update-student/<?php echo $student_info->id ?>" method="post">
						  <fieldset>
							<div class="control-group">
							  <label class="control-label" for="date01">Student Name</label>
							  <div class="controls">
								<input type="text" class="input-xlarge" name="name" value="<?php echo $student_info->name ?>" >
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="date01">Student Phone</label>
							  <div class="controls">
								<input type="text" class="input-xlarge" name="phone" value="<?php echo $student_info->phone ?>" >
							  </div>
							</div>
							<div class="control-group">
							  <label class="control-label" for="date01">Student Roll</label>
							  <div class="controls">
								<input type="text" class="input-xlarge" name="roll" value="<?php echo $student_info->roll ?>" >
							  </div>
							</div>

							<!-- <div class="control-group">
							  <label class="control-label" for="fileInput">File input</label>
							  <div class="controls">
								<input class="input-file uniform_on" id="fileInput" type="file">
							  </div>
							</div>          
							<div class="control-group hidden-phone">
							  <label class="control-label" for="textarea2">Textarea WYSIWYG</label>
							  <div class="controls">
								<textarea class="cleditor" id="textarea2" rows="3"></textarea>
							  </div>
							</div> -->

							<div class="form-actions">
							  <button type="submit" class="btn btn-primary">Update Student</button>
							  <button type="reset" class="btn">Cancel</button>
							</div>
						  </fieldset>
						</form>   

					</div>
				</div><!--/span-->