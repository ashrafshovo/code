<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentModel extends CI_Model {

	public function save_student_info()
	{
		$data = array();
		$dat = array();
		$dat['name'] = $this->input->post('name', true);
		$dat['phone'] = $this->input->post('phone', true);
		$dat['roll'] = $this->input->post('roll', true);

		if ($dat['name'] != Null && $dat['phone'] != Null && $dat['roll'] != Null) {
			
			$this->db->insert('student', $dat);

			$data['success_message'] = 'Student added successfully.';
			$this->session->set_userdata($data);
		}
		else{
			$data['error_message'] = 'All fields are required.';
			$this->session->set_userdata($data);
		}
	}

	public function all_student_info()
	{
		$this->db->select('*');
		$this->db->from('student');
		$query_result = $this->db->get();
		$student_info = $query_result->result();

		return $student_info;	
	}

	public function student_info($id)
	{
		$this->db->select('*');
		$this->db->from('student');
		$this->db->where('id', $id);
		$query_result = $this->db->get();
		$result = $query_result->row();

		return $result;
	}

	public function update_student_info($id)
	{
		$data = array();
		$dat = array();
		$dat['name'] = $this->input->post('name', true);
		$dat['phone'] = $this->input->post('phone', true);
		$dat['roll'] = $this->input->post('roll', true);
		
		if ($dat['name'] != Null && $dat['phone'] != Null && $dat['roll'] != Null) {
			$this->db->where('id', $id);
			$this->db->update('student', $dat);
			$this->session->unset_userdata($dat);

			$data['success_message'] = 'Student updated successfully.';
			$this->session->set_userdata($data);
		}else {
			$this->session->unset_userdata($dat);

			$data['error_message'] = 'All fields are required.';
			$this->session->set_userdata($data);
			
			redirect('edit-student/'.$id);
		}
	}

	public function delete_student_info($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('student');
		
		$data['success_message'] = 'Student deleted successfully.';
		$this->session->set_userdata($data);

	}
	
}
