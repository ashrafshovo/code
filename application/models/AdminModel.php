<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model {

	public function admin_info($email, $password)
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('email', $email);
		$this->db->where('password', md5($password));
		$query_result = $this->db->get();
		$result = $query_result->row();
		
		//print_r($result);

		return $result;
	}

	public function save_admin()
	{
		$dat = array();
		$data = array();

		$dat['name'] = $this->input->post('name', true);
		$dat['email'] = $this->input->post('email', true);
		$password = $this->input->post('password', true);
		$dat['password'] = md5($password);
		
			$img_data = array();
			$error = "";
			$config['upload_path']			= 'image/';
			$config['allowed_types']		= 'gif|jpg|png';
			$config['max_size']				= 100000;
			$config['max_width']			= 2048;
			$config['max_height']			= 1024;

			$this->load->library('upload', $config);
			
			if ($dat['name'] && $dat['email'] && $dat['password']) {
				if ( ! $this->upload->do_upload('image')) {
					$error = $this->upload->display_errors();		
				}else {
					$img_data = $this->upload->data();
					$dat['image'] = $config['upload_path'].$img_data['file_name'];

					$this->db->insert('admin', $dat);

					$data['success_message'] = 'Admin added successfully.';
					$this->session->set_userdata($data);
				}
			}else{
				$data['error_message'] = 'All fields are required.';
				$this->session->set_userdata($data);
			}
	}

	public function all_admin_info()
	{
		$this->db->select('*');
		$this->db->from('admin');
		$query_result = $this->db->get();
		$result = $query_result->result();

		return $result;
	}
	
}
