<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller {

	public function index()
	{
		$data['title'] = 'CodeIgniter || Login';
		$this->load->view('login', $data);
	}

	
	public function admin_login()
	{
		$email = $this->input->post('email', true);
		$password = $this->input->post('password', true);

		$this->load->model('AdminModel');
		$result = $this->AdminModel->admin_info($email, $password);

		$data = array();

		if ($result) {
			$data['id'] = $result->id;
			$data['name'] = $result->name;
			$data['email'] = $result->email;
			$data['title'] = 'CodeIgniter || Dashboard';

			$this->session->set_userdata($data);
			redirect('dashboard');
		}else {

			$data['error_message'] = 'Your email or password is invalid.';
			$this->session->set_userdata($data);

			redirect(base_url());
		}

		/*$data=array();
		$data['content']=$this->load->view('section/admin_index', '', true);
		$this->load->view('dashboard', $data);*/
	}

	public function logout()
	{
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('name');
		$this->session->unset_userdata('email');

		$data['success_message'] = 'You have logged out successfully.';
		$this->session->set_userdata($data);

		redirect(base_url());
	}

	public function admin_dashboard()
	{
		$data=array();
		$data['title'] = 'CodeIgniter || Dashboard';
		$data['content']=$this->load->view('section/admin_index', '', true);
		$this->load->view('dashboard', $data);
	}

	public function admin_profile()
	{
		$data=array();
		$data['title'] = 'CodeIgniter || Admin Profile';
		$data['content']=$this->load->view('section/admin_profile', '', true);
		$this->load->view('dashboard', $data);
	}

	public function add_student()
	{
		$data=array();
		$data['title'] = 'CodeIgniter || Add Student';
		$data['content']=$this->load->view('section/add_student', '', true);
		$this->load->view('dashboard', $data);
	}
	
	/*
	public function manage_student()
	{
		$data=array();
		$data['title'] = 'CodeIgniter || Manage Student';
		$this->load->model('StudentModel');
		$data['all_student_info'] = $this->StudentModel->all_student_info();
		$data['content']=$this->load->view('section/manage_student', '', true);
		$this->load->view('dashboard', $data);
	}
	*/
	
	public function admin_add()
	{
		$data=array();
		$data['title'] = 'CodeIgniter || Add Admin';
		$data['content']=$this->load->view('section/admin_add', '', true);
		$this->load->view('dashboard', $data);
	}

	public function admin_save()
	{
		$this->load->model('AdminModel');
		$this->AdminModel->save_admin();

		redirect ('admin-add');
	}

	public function manage_admin()
	{
		$data=array();
		$data['title'] = 'CodeIgniter || Manage Admin';
		$this->load->model('AdminModel');
		$data['all_admin_info'] = $this->AdminModel->all_admin_info();
		$data['content']=$this->load->view('section/admin_all', $data, true);
		$this->load->view('dashboard', $data);
	}
}
