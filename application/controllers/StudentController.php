<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StudentController extends CI_Controller {

	public function save()
	{
		$this->load->model('StudentModel');
		$this->StudentModel->save_student_info();
		
		redirect('add-student');
	}
	public function index()
	{
		$data = array();
		$data['title'] = 'CodeIgniter || Manage Student';
		$this->load->model('StudentModel');
		$data['all_student_info'] = $this->StudentModel->all_student_info();
		$data['content']=$this->load->view('section/manage_student', $data, true);
		$this->load->view('dashboard', $data);
	}

	public function view_student($id)
	{
		$data = array();
		$data['title'] = 'CodeIgniter || View Student';
		$this->load->model('StudentModel');
		$data['student_info'] = $this->StudentModel->student_info($id);
		$data['content']=$this->load->view('section/view_student', $data, true);
		$this->load->view('dashboard', $data);
	}

	public function edit_student($id)
	{
		$data = array();
		$data['title'] = 'CodeIgniter || Edit Student';
		$this->load->model('StudentModel');
		$data['student_info'] = $this->StudentModel->student_info($id);
		$data['content']=$this->load->view('section/edit_student', $data, true);
		$this->load->view('dashboard', $data);
	}

	public function update_student($id)
	{
		$this->load->model('StudentModel');

		$this->StudentModel->update_student_info($id);
		
		redirect ('manage-student');
	}

	public function delete_student($id)
	{
		$this->load->model('StudentModel');
		
		$this->StudentModel->delete_student_info($id);
		
		redirect('manage-student');
	}

}
